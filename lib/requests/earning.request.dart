import 'package:transportwiz_driver/constants/api.dart';
import 'package:transportwiz_driver/models/api_response.dart';
import 'package:transportwiz_driver/models/currency.dart';
import 'package:transportwiz_driver/models/earning.dart';
import 'package:transportwiz_driver/services/http.service.dart';

class EarningRequest extends HttpService {
  //
  Future<List<dynamic>> getEarning() async {
    final apiResult = await get(Api.earning);

    //
    final apiResponse = ApiResponse.fromResponse(apiResult);
    if (apiResponse.allGood) {
      return [
        Currency.fromJSON(apiResponse.body["currency"]),
        Earning.fromJson(apiResponse.body["earning"]),
      ];
    } else {
      throw apiResponse.message;
    }
  }
}
