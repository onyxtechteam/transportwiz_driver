import 'package:transportwiz_driver/constants/api.dart';
import 'package:transportwiz_driver/models/api_response.dart';
import 'package:transportwiz_driver/models/order.dart';
import 'package:transportwiz_driver/services/http.service.dart';

class TaxiRequest extends HttpService {
  //
  Future<Order> getOnGoingTrip() async {
    final apiResult = await get(
      "${Api.currentTaxiBooking}",
    );
    //
    final apiResponse = ApiResponse.fromResponse(apiResult);
    //
    if (apiResponse.allGood) {
      //if there is order
      return Order.fromJson(apiResponse.body["order"]);
    }

    //
    throw apiResponse.body;
  }

  //
  Future<ApiResponse> cancelTrip(int id) async {
    final apiResult = await get(
      "${Api.cancelTaxiBooking}/$id",
    );
    //
    return ApiResponse.fromResponse(apiResult);
  }

  Future<ApiResponse> rateUser(
    int orderId,
    int userId,
    double newTripRating,
    String review,
  ) async {
    //
    final apiResult = await post(
      "${Api.rating}",
      {
        //
        "user_id": userId,
        "order_id": orderId,
        "rating": newTripRating,
        "review": review,
      },
    );
    //
    return ApiResponse.fromResponse(apiResult);
  }
}
