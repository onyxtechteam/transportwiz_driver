import 'package:flutter/material.dart';
import 'package:transportwiz_driver/constants/app_colors.dart';
import 'package:transportwiz_driver/constants/app_images.dart';
import 'package:transportwiz_driver/services/validator.service.dart';
import 'package:transportwiz_driver/view_models/login.view_model.dart';
import 'package:transportwiz_driver/widgets/base.page.dart';
import 'package:transportwiz_driver/widgets/buttons/custom_button.dart';
import 'package:transportwiz_driver/widgets/custom_text_form_field.dart';

import 'package:stacked/stacked.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_driver/translations/login.i18n.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<LoginViewModel>.reactive(
      viewModelBuilder: () => LoginViewModel(context),
      onModelReady: (model) => model.initialise(),
      builder: (context, model, child) {
        return BasePage(
          body: SafeArea(
            top: true,
            bottom: false,
            child: VStack(
              [
                Image.asset(
                  AppImages.onboarding1,
                ).hOneForth(context).centered(),
                //
                VStack(
                  [
                    //
                    "Welcome Back".i18n.text.xl2.semiBold.make(),
                    "Login to continue".i18n.text.light.make(),

                    //form
                    Form(
                      key: model.formKey,
                      child: VStack(
                        [
                          //
                          CustomTextFormField(
                            labelText: "Email",
                            keyboardType: TextInputType.emailAddress,
                            textEditingController: model.emailTEC,
                            validator: FormValidator.validateEmail,
                          ).py12(),
                          CustomTextFormField(
                            labelText: "Password".i18n,
                            obscureText: true,
                            textEditingController: model.passwordTEC,
                            validator: FormValidator.validatePassword,
                          ).py12(),

                          //
                          "Forgot Password ?"
                              .i18n
                              .text
                              .underline
                              .make()
                              .onInkTap(
                                model.openForgotPassword,
                              ),
                          //
                          CustomButton(
                            title: "Login".i18n,
                            loading: model.isBusy,
                            onPressed: model.processLogin,
                          ).centered().py12(),
                        ],
                        crossAlignment: CrossAxisAlignment.end,
                      ),
                    ).py20(),

                    //registration link
                    "Become a partner"
                        .i18n
                        .toUpperCase()
                        .text
                        .color(AppColor.primaryColor)
                        .underline
                        .semiBold
                        .makeCentered()
                        .onInkTap(model.openRegistrationlink),
                  ],
                )
                    .wFull(context)
                    .p20()
                    .scrollVertical()
                    .box
                    .color(context.cardColor)
                    .make()
                    .expand(),

                //
              ],
            ).pOnly(
              bottom: context.mq.viewInsets.bottom,
            ),
          ),
        );
      },
    );
  }
}
