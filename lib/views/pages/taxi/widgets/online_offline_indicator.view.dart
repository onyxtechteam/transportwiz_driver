import 'package:flutter/material.dart';
import 'package:transportwiz_driver/constants/app_colors.dart';
import 'package:transportwiz_driver/view_models/taxi/taxi.vm.dart';
import 'package:transportwiz_driver/widgets/busy_indicator.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:transportwiz_driver/translations/taxi.i18n.dart';

class OnlineOfflineIndicatorView extends StatelessWidget {
  const OnlineOfflineIndicatorView(this.vm, {Key key}) : super(key: key);
  final TaxiViewModel vm;
  @override
  Widget build(BuildContext context) {
    return Visibility(
      child: SafeArea(
        child: VStack(
          [
            //
            !vm.busy(vm.appService.driverIsOnline) ? ToggleSwitch(
              minWidth: 90.0,
              initialLabelIndex: vm.appService.driverIsOnline ? 0 : 1,
              cornerRadius: 20.0,
              activeFgColor: Colors.white,
              inactiveBgColor: context.backgroundColor,
              inactiveFgColor: context.textTheme.bodyText1.color,
              totalSwitches: 2,
              labels: ['Online'.i18n, 'Offline'.i18n],
              activeBgColors: [
                [AppColor.primaryColor],
                [AppColor.primaryColor]
              ],
              onToggle: (index) {
                vm.newTaxiBookingService.toggleVisibility(index == 0);
              },
            ).py16().centered(): BusyIndicator().py16().centered(),
           
          ],
        ),
      ),
    );
  }
}
