import 'package:flutter/material.dart';
import 'package:transportwiz_driver/constants/app_colors.dart';
import 'package:transportwiz_driver/view_models/taxi/taxi.vm.dart';
import 'package:transportwiz_driver/views/pages/taxi/widgets/ongoing_trip.view.dart';
import 'package:transportwiz_driver/views/pages/taxi/widgets/online_offline_indicator.view.dart';
import 'package:transportwiz_driver/widgets/base.page.dart';
import 'package:transportwiz_driver/widgets/busy_indicator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:stacked/stacked.dart';
import 'package:velocity_x/velocity_x.dart';

class TaxiOrderPage extends StatelessWidget {
  const TaxiOrderPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BasePage(
      body: ViewModelBuilder<TaxiViewModel>.reactive(
        viewModelBuilder: () => TaxiViewModel(context),
        onModelReady: (vm) => vm.initialise(),
        builder: (context, vm, child) {
          return Stack(
            children: [
              //google map
              GoogleMap(
                initialCameraPosition: CameraPosition(
                  target: LatLng(0.00, 0.00),
                ),
                myLocationButtonEnabled: true,
                myLocationEnabled: true,
                onMapCreated: vm.onMapReady,
                padding: vm.googleMapPadding,
                markers: vm.gMapMarkers,
                polylines: vm.gMapPolylines,
              ),

              //trip details view
              OnGoingTripView(vm),

              //Online/offline
              OnlineOfflineIndicatorView(vm),

              //loading
              Visibility(
                visible: vm.isBusy,
                child: BusyIndicator(
                  color: AppColor.primaryColor,
                ).wh(60, 60)
                    .box
                    .white
                    .rounded
                    .p32
                    .makeCentered()
                    .box
                    .color(Colors.black.withOpacity(0.3))
                    .make()
                    .wFull(context)
                    .hFull(context),
              ),
            ],
          );
        },
      ),
    );
  }
}
