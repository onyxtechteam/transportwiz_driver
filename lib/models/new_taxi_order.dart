// To parse this JSON data, do
//
//     final newTaxiOrder = newTaxiOrderFromJson(jsonString);

import 'dart:convert';

import 'package:transportwiz_driver/services/location.service.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:supercharged/supercharged.dart';

NewTaxiOrder newTaxiOrderFromJson(String str) =>
    NewTaxiOrder.fromJson(json.decode(str));

String newTaxiOrderToJson(NewTaxiOrder data) => json.encode(data.toJson());

class NewTaxiOrder {
  NewTaxiOrder({
    this.pickup,
    this.status,
    this.driverId,
    this.id,
    this.code,
    this.vehicleTypeId,
    this.tripDistance,
    this.dropoff,
    this.earthDistance,
  });

  List<String> pickup;
  String status;
  dynamic driverId;
  int id;
  String code;
  int vehicleTypeId;
  double tripDistance;
  List<String> dropoff;
  double earthDistance;

  factory NewTaxiOrder.fromJson(Map<String, dynamic> json) => NewTaxiOrder(
        pickup: List<String>.from(json["pickup"].map((x) => x)),
        status: json["status"],
        driverId: json["driver_id"],
        id: json["id"],
        code: json["code"],
        vehicleTypeId: json["vehicle_type_id"],
        tripDistance: json["trip_distance"].toDouble(),
        dropoff: List<String>.from(json["dropoff"].map((x) => x)),
        earthDistance: json["earth_distance"].toDouble(),
      );

  double get pickupDistance {
    LocationService.currentLocation;
    return Geolocator.distanceBetween(
      LocationService.currentLocation?.latitude,
      LocationService.currentLocation?.longitude,
      pickup[1].toDouble(),
      pickup[2].toDouble(),
    );
  }

  Map<String, dynamic> toJson() => {
        "pickup": List<dynamic>.from(pickup.map((x) => x)),
        "status": status,
        "driver_id": driverId,
        "id": id,
        "code": code,
        "vehicle_type_id": vehicleTypeId,
        "trip_distance": tripDistance,
        "dropoff": List<dynamic>.from(dropoff.map((x) => x)),
        "earth_distance": earthDistance,
      };
}
