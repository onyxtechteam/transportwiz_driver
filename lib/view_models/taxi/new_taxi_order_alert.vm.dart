import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter/material.dart';
import 'package:transportwiz_driver/models/new_taxi_order.dart';
import 'package:transportwiz_driver/requests/order.request.dart';
import 'package:transportwiz_driver/services/app.service.dart';
import 'package:transportwiz_driver/view_models/base.view_model.dart';
import 'package:velocity_x/velocity_x.dart';

class NewTaxiOrderAlertViewModel extends MyBaseViewModel {
  //
  OrderRequest orderRequest = OrderRequest();
  NewTaxiOrder newOrder;
  bool canDismiss = false;
  CountDownController countDownTimerController = CountDownController();
  NewTaxiOrderAlertViewModel(this.newOrder, BuildContext context) {
    this.viewContext = context;
  }

  initialise() {
    //
    AppService().playNotificationSound();
    //
    countDownTimerController.start();
  }

  void processOrderAcceptance() async {
    setBusy(true);
    try {
      final order = await orderRequest.acceptNewOrder(
        newOrder.id,
        status: "preparing",
      );
      AppService().assetsAudioPlayer.stop();
      //
      viewContext.pop(order);
      // return;
    } catch (error) {
      viewContext.showToast(
        msg: "$error",
        bgColor: Colors.red,
        textColor: Colors.white,
        textSize: 20,
      );

      //
      canDismiss = true;
    }
    setBusy(false);
    //
    if (canDismiss) {
      AppService().assetsAudioPlayer.stop();
      viewContext.pop();
    }
  }

  void countDownCompleted(bool started) {
    print('Countdown Ended');
    if (started) {
      if (isBusy) {
        canDismiss = true;
      } else {
        AppService().assetsAudioPlayer.stop();
        viewContext.pop();
        //STOP NOTIFICATION SOUND
        AppService().stopNotificationSound();
      }
    }
  }
}
