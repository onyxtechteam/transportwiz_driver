import 'dart:io';

import 'package:firebase_chat/firebase_chat.dart';
import 'package:firebase_chat/models/chat_entity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:transportwiz_driver/constants/app_strings.dart';
import 'package:transportwiz_driver/models/delivery_address.dart';
import 'package:transportwiz_driver/services/app.service.dart';
import 'package:transportwiz_driver/services/location.service.dart';
import 'package:transportwiz_driver/views/pages/payment/custom_webview.page.dart';
import 'package:location/location.dart';
import 'package:stacked/stacked.dart';
import 'package:firestore_repository/firestore_repository.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:velocity_x/velocity_x.dart';

class MyBaseViewModel extends BaseViewModel {
  //
  BuildContext viewContext;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final currencySymbol = AppStrings.currencySymbol;
  DeliveryAddress deliveryaddress = DeliveryAddress();
  String firebaseVerificationId;
  ChatEntity chatEntity;

  //

  void initialise() {
    FirestoreRepository();
  }

  newFormKey() {
    formKey = GlobalKey<FormState>();
    notifyListeners();
  }

  //
  void startNewOrderBackgroundService() {
    WidgetsFlutterBinding.ensureInitialized();

    //
    //try sending location to fcm
    if (LocationService.currentLocation != null) {
      print("Resending fcm location");
      LocationService.syncLocationWithFirebase(LocationData.fromMap({
        "latitude": LocationService.currentLocation.latitude,
        "longitude": LocationService.currentLocation.longitude,
      }));
    }
  }

  //
  openWebpageLink(String url) async {
    //
    if (Platform.isIOS) {
      launch(url);
      return;
    }
    await AppService().navigatorKey.currentContext.push(
          (context) => CustomWebviewPage(
            selectedUrl: url,
          ),
        );

    //
  }

  //show toast
  toastSuccessful(String msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 2,
      backgroundColor: Colors.green,
      textColor: Colors.white,
      fontSize: 14.0,
    );
  }

  toastError(String msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 2,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 14.0,
    );
  }
}
