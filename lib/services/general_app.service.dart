import 'dart:async';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:transportwiz_driver/services/firebase.service.dart';

import 'background_order.service.dart';

class GeneralAppService {
  //

//Hnadle background message
  static Future<void> onBackgroundMessageHandler(RemoteMessage message) async {
    await Firebase.initializeApp();
    FirebaseService().saveNewNotification(message);
    BackgroundOrderService().processNotificationForNewOrderAlert(
      message,
      appIsInBackground: true,
    );
  }
}
