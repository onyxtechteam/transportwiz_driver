import 'dart:async';
import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:transportwiz_driver/constants/app_strings.dart';
import 'package:transportwiz_driver/models/new_taxi_order.dart';
import 'package:transportwiz_driver/services/app.service.dart';
import 'package:transportwiz_driver/services/auth.service.dart';
import 'package:transportwiz_driver/services/location.service.dart';
import 'package:transportwiz_driver/view_models/taxi/taxi.vm.dart';
import 'package:transportwiz_driver/views/pages/taxi/widgets/incoming_new_order_alert.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:supercharged/supercharged.dart';

class NewTaxiBookingService {
  TaxiViewModel taxiViewModel;
  NewTaxiBookingService(this.taxiViewModel);
  StreamSubscription myLocationListener;
  Location location = new Location();
  bool showNewTripView = false;
  CountDownController countDownTimerController = CountDownController();
  GlobalKey newAlertViewKey = GlobalKey<FormState>();
  //
  FirebaseFirestore firebaseFireStore = FirebaseFirestore.instance;
  StreamSubscription newOrderStreamSubscription;
  StreamSubscription locationStreamSubscription;

  //dispose
  void dispose() {
    myLocationListener?.cancel();
    newOrderStreamSubscription?.cancel();
  }

  //
  zoomToCurrentLocation() async {
    myLocationListener?.cancel();
    final currentPosition = await Geolocator.getCurrentPosition();
    if (currentPosition != null) {
      zoomToLocation(currentPosition.latitude, currentPosition.longitude);
    }
    //
    myLocationListener =
        LocationService.location.onLocationChanged.listen((locationData) {
      //actually zoom now
      zoomToLocation(locationData.latitude, locationData.longitude);
    });
  }

  //
  zoomToLocation(double lat, double lng) {
    taxiViewModel.googleMapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(lat, lng),
          zoom: 16,
        ),
      ),
    );
  }

  //
  zoomToPickupLocation() async {
    //
    taxiViewModel.clearMapMarkers();
    taxiViewModel.gMapMarkers.add(
      Marker(
        markerId: MarkerId('sourcePin'),
        position: LatLng(
          taxiViewModel.newOrder.pickup[1].toDouble(),
          taxiViewModel.newOrder.pickup[2].toDouble(),
        ),
        icon: taxiViewModel.sourceIcon,
        anchor: Offset(0.5, 0.5),
      ),
    );
    //
    taxiViewModel.notifyListeners();
    //actually zoom now
    taxiViewModel.googleMapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            taxiViewModel.newOrder.pickup[1].toDouble(),
            taxiViewModel.newOrder.pickup[2].toDouble(),
          ),
          zoom: 16,
        ),
      ),
    );
  }

  //
  toggleVisibility(bool value) async {
    //
    taxiViewModel.appService.driverIsOnline = value;
    final updated = await taxiViewModel.syncDriverNewState();
    //
    if (updated) {
      if (value && taxiViewModel.onGoingOrderTrip == null) {
        startNewOrderListener();
      } else {
        stopListeningToNewOrder();
      }
    }
  }

  //start lisntening for new orders
  startNewOrderListener() {
    //

    //
    locationStreamSubscription =
        LocationService.driverLocationEarthDistance.listen(
      (value) {
        //
        double driverEarthDistanceSouth = value - AppStrings.driverSearchRadius;
        double driverEarthDistanceNorth = value + AppStrings.driverSearchRadius;
        //if new order is allowed
        listenForNewOrder(driverEarthDistanceSouth, driverEarthDistanceNorth);
      },
    );
    //
  }

  listenForNewOrder(
    double southBound,
    double northBound,
  ) {
    //
    // print("South ==> $southBound;;;; North ===> $northBound");
    newOrderStreamSubscription = firebaseFireStore
        .collection("orders")
        .where("earth_distance", isGreaterThanOrEqualTo: southBound)
        .where("earth_distance", isLessThanOrEqualTo: northBound)
        .orderBy("earth_distance")
        .limit(AppStrings.maxDriverOrderAtOnce)
        .snapshots()
        .listen(
      (event) {
        //
        //show about the new order alert
        final driverVehicle = AuthServices.driverVehicle;
        for (var doc in event.docs) {
          //
          bool isDriverVehicleType =
              driverVehicle.vehicleTypeId == doc.data()["vehicle_type_id"];
          //
          if (doc != null && isDriverVehicleType) {
            //
            stopListeningToNewOrder();
            showNewOrderAlert(doc.data());
            break;
          } else {
            print("not driver vehicle type");
            print("Driver vehicle ==> ${driverVehicle.toJson()}");
          }
        }
      },
    );
  }

  //stop listening to new orders
  stopListeningToNewOrder() {
    locationStreamSubscription?.cancel();
    newOrderStreamSubscription?.cancel();
  }

  //
  showNewOrderAlert(Map<String, dynamic> data) async {
    //
    try {
      taxiViewModel.newOrder = NewTaxiOrder.fromJson(data);
      zoomToPickupLocation();
      final result = await showModalBottomSheet(
        isDismissible: false,
        context: taxiViewModel.viewContext,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return IncomingNewOrderAlert(taxiViewModel, taxiViewModel.newOrder);
        },
      );
      //
      if (result != null) {
        taxiViewModel.onGoingOrderTrip = result;
        taxiViewModel.onGoingTaxiBookingService.loadTripUIByOrderStatus();
        taxiViewModel.notifyListeners();
      }
    } catch (error) {
      print("show new order alert error ==> $error");
    }
  }

  void countDownCompleted() {
    countDownTimerController?.pause();
    AppService().stopNotificationSound();
    showNewTripView = false;
    zoomToCurrentLocation();
    taxiViewModel.notifyListeners();
  }

  void processOrderAcceptance() {}
}
